#include <curl/curl.h>
#include <jsoncpp/json/json.h>
#include <memory>
#include <cstdint>
#include <fstream>
#include <iomanip>
#include "csv.h"
#include <algorithm>
#include <ctime>
#include <cctype>
#include <unistd.h>
#include <thread>
#include <sys/ioctl.h> // For FIONREAD
#include <termios.h>
#include <stdbool.h>

#include "funkcje.h"
#include "makra.h"

namespace
{
	int isKeyboardHit(void) {
		static bool initflag = false;
		static const int STDIN = 0;

		if (!initflag) {
			// Use termios to turn off line buffering
			struct termios term;
			tcgetattr(STDIN, &term);
			term.c_lflag &= ~ICANON;
			tcsetattr(STDIN, TCSANOW, &term);
			setbuf(stdin, NULL);
			initflag = true;
		}

		int nbbytes;
		ioctl(STDIN, FIONREAD, &nbbytes);  // 0 is STDIN
		return nbbytes;
	}
	
    std::size_t callback(const char* in, std::size_t size, std::size_t num, std::string* out){
        const std::size_t totalBytes(size * num);
        out->append(in, totalBytes);
        return totalBytes;
    }


	void readFromCSVFile(std::vector<std::string> &stopName, std::vector<std::string> &stopNumber,std::vector<int> &stopId){
		io::CSVReader<3> stops("przystanki.csv");
		stops.read_header(io::ignore_extra_column, "Nazwa", "Numer", "Id");
		std::string name, number;
		int id;
		while(stops.read_row(name, number, id)){
			stopName.push_back(name);
			stopNumber.push_back(number);
			stopId.push_back(id);
		}
	}
	
	void findAllStops(std::vector<size_t> &positions, std::vector<std::string> stopName, std::string toFind){
		size_t position = 0;
		for(std::string name : stopName){
			size_t found = name.find(toFind);
				if(found != std::string::npos)
					positions.push_back(position);
			++position;
		}
	}

	std::string findStopId(std::vector<std::string> stopName, std::vector<std::string> stopNumber, std::vector<int> stopId, size_t &idOfStop){
		std::vector<size_t> found;
		while(found.size() == 0){
			system("clear");
			std::cout<<"Wpisz co najmniej 3 znaki i wybierz przystanek"<<std::endl;
			std::string stopName_;
			do{
				std::cin>>stopName_;
			}while(stopName_.length() < 3);
			
			system("clear");
			stopName_[0] = toupper(stopName_[0]);
			findAllStops(found, stopName, stopName_);
			if(found.size() == 0){
				std::cout<<RED<<"Nie udało się znaleźć przystanku spełniającego kryteria. Spróbuj ponownie"<<RESET<<std::endl;
				std::this_thread::sleep_for(std::chrono::milliseconds(1500));
			}
		}
		system("clear");
		std::cout<<"ID\t"<<"Nazwa przystanku"<<std::endl;
		for(size_t index : found){
			std::cout<<index<<"\t"<<stopName[index] <<" "<< stopNumber[index]<<std::endl;
		}
		std::cout<<std::endl<<"Podaj ID przystanku"<<std::endl;
		std::cin>>idOfStop;	
		found.clear();
	return std::to_string(stopId[idOfStop]);
	}
	
	void drawHorizontalLine(){
		for(int i=0; i<55; ++i) //55 is line width
			std::cout<<"-";
	}
	
	void refreshBoard(std::vector<std::string> stopName, std::vector<std::string> stopNumber, size_t &idOfStop, Json::Value jsonData){
		system("clear");
        time_t timeNow = time(0);
        char* currentTime = ctime(&timeNow);
        std::cout<<"\t\t\t"<<stopName[idOfStop]<<" "<< stopNumber[idOfStop]<<std::endl;
        std::cout<<"Linia nr|"<<std::setw(25)<<"Kierunek"<<"\t"<<std::setw(8)<<"|"<<"Odjazd"<<std::endl;
        drawHorizontalLine();
		std::cout<<std::endl;
		if(jsonData["delay"].size() ==0)
			std::cout<<WHITE<<"\t\t\tBRAK KURSÓW"<<RESET<<std::endl;
		else{
			for (unsigned int index =0; index<jsonData["delay"].size(); ++index){
				if(jsonData["delay"][index]["delayInSeconds"] > 0)
					std::cout << RED <<std::setw(8)<< jsonData["delay"][index]["routeId"].asString()<<"|"<<std::setw(25)<< jsonData["delay"][index]["headsign"].asString()<<"\t"<<std::setw(8)<<"|"<< jsonData["delay"][index]["estimatedTime"].asString()<< RESET << std::endl;
				else
					std::cout << BOLDGREEN << std::setw(8)<< jsonData["delay"][index]["routeId"].asString()<<"|"<<std::setw(25)<< jsonData["delay"][index]["headsign"].asString()<<"\t"<<std::setw(8)<<"|"<< jsonData["delay"][index]["estimatedTime"].asString()<< RESET << std::endl;
			}
		}
		drawHorizontalLine();
		std::cout<<std::endl<<"Stan na: "<<currentTime<<std::endl;
		//int c = getchar();
	}
	
	void printMarked(std::string text){
		std::cout << BACKG <<text<<RESET<<std::endl;
	}
	
	
	int showMainMenuAndTellWhatToDo(){
		size_t chosen = 0;
		int c,c1,c2;
		std::string text[]={"1. Sprawdź rozkład na wybranym przystanku", "2. Instrukcja korzystania", "3. Wyjdz z programu"};
		while(c!=ENTER && c1!=ENTER && c2!=ENTER){
			system("clear");
			std::cout<<"System Informacji Pasażerskiej Gdańsk"<<std::endl;
			std::cout<<"Co chcesz zrobić?"<<std::endl;
			for(size_t row=0; row < 3; ++row){
				if(row==chosen)
					printMarked(text[row]);
				else
					std::cout<<text[row]<<std::endl;
			}
			while (!isKeyboardHit()){ /* do nothing */}
			
			if((c=getchar())==ENTER)
				return chosen;
				
			c1 = getchar();
			c2 = getchar();
			
			switch(c2){
				case KEY_UP:
					chosen >= 1 ? chosen-- : chosen;
					break;
				case KEY_DOWN:
					chosen < 2 ? chosen++ : chosen;
					break;
				default:
					break;
			}
		}
		return chosen;
	}
	
	void showInstruction(){
		system("clear");
		std::cout<<WHITE<<"Instrukcja korzystania:"<<std::endl;
		std::cout<<"Wybierz opcję \"Sprawdź rozkład na wybranym przystanku\", nastepnie wpisz conajmniej 3 znaki z nazwy przystanku (wielkość liter nie ma znaczenia)."<<std::endl;
		std::cout<<"Wybierz interesujący Cię przystanek z listy i zatwierdź klawiszem enter."<<std::endl;
		std::cout<<"Miłego korzystania!"<<std::endl;
		std::cout<<RESET<<std::endl;
		std::cout<<std::endl<<std::endl<<"Aby wrocic wcisnij dowolny klawisz"<<std::endl;
		int iAmOnlyWaitingForUserBeingReady = getchar();
	}
}
