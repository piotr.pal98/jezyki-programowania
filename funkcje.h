#include <iostream>
#include <vector>
#include <cstring>

namespace{
	int isKeyboardHit(void);
	std::size_t callback(const char* in,std::size_t size,std::size_t num, std::string* out);
	void readFromCSVFile(std::vector<std::string> &stopName, std::vector<std::string> &stopNumber,std::vector<int> &stopId);
	void findAllStops(std::vector<size_t> &positions, std::vector<std::string> stopName, std::string toFind);
	std::string findStopId(std::vector<std::string> stopName, std::vector<std::string> stopNumber, std::vector<int> stopId, size_t &idOfStop);
	void drawHorizontalLine();
	void refreshBoard(std::vector<std::string> stopName, std::vector<std::string> stopNumber, size_t &idOfStop, Json::Value jsonData);
	void printMarked(std::string text);
	int showMainMenuAndTellWhatToDo();
	void showInstruction();
}
